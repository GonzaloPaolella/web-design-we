var form = document.querySelector('#contact-form');

form.addEventListener('submit', function(event) {
  event.preventDefault();
  let formData = new FormData(form);
  fetch("contact.php", {
      body: formData,
      method: "POST",
  });
  swal({
    title: "¡Muchas Gracias!",
    text: "Tu consulta fue recibida con éxito",
    icon: "success",
    button: "Salir",
  });
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({
  'event': 'Lead'
  });
  form.reset();
});