<!doctype html>
<html lang="en">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-55F9KKZ');</script>
    <!-- End Google Tag Manager -->

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Diseño Web | We Management</title>

   <!-- FAVI ICON -->
   <link rel="icon" type="image/png" href="assets/images/favicon.png" sizes="32x32">
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <!-- ALL GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:300,400,500,600,700,800,900&subset=latin-ext" rel="stylesheet">
    <!-- FONT AWESOME CSS -->
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <!-- OWL CAROSEL CSS -->
    <link rel="stylesheet" href="assets/owlcarousel/css/owl.carousel.css">
    <link rel="stylesheet" href="assets/owlcarousel/css/owl.theme.css">
    <!-- LIGHTBOX CSS -->
    <link rel="stylesheet" href="assets/css/lightbox.min.css">
    <!-- YOUTUBE CSS -->
    <link rel="stylesheet" href="assets/css/YTPlayer.css">
    <!-- MAGNIFIC CSS -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <!-- ANIMATE CSS -->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/component.css">
    <link rel="stylesheet" href="assets/css/set2.css">
    <!-- MAIN STYLE CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- RESPONSIVE CSS -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- SWEET ALERT -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  </head>
  <body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55F9KKZ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="btn-whatsapp">
        <a href="https://api.whatsapp.com/send?phone=5491166641537" target="_blank">
        <img src="assets/images/btn_whatsapp.png" alt="">
        </a>
    </div>

    <header class="welcome-area" id="home">
        <div class="header-top-information-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-top-info-left">
                            <p>hola@wemanagement.com.ar</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="header-top-info-right">
                            <ul class="header-social">
                                <li><a href="https://www.facebook.com/wemanagement.ok"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href="https://www.instagram.com/wemanagement.ok/"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li><a href="https://www.linkedin.com/company/wemanagement/"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" style="display: flex; align-items: center;">
                        <!-- START LOGO DESIGN AREA -->
                        <div class="logo">
                            <a href="">WE DESIGN</a>
                        </div>
                        <!-- END LOGO DESIGN AREA -->
                    </div>
                    <div class="col-md-9">
                        <!-- START MENU DESIGN AREA -->
                        <div class="mainmenu">
                            <div class="navbar navbar-nobg">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="positon: absolute; top:: 0; right: 0px;">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right" style="display: inline-block;">
                                        <li class="active"><a class="smoth-scroll" href="#home">Inicio <div class="ripple-wrapper"></div></a>
                                        </li>
                                        <!-- <li><a class="smoth-scroll" href="#about">Nosotros</a>
                                        </li> -->
                                        <li><a class="smoth-scroll" href="#service">Servicios</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="#portfolio">Portfolio</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="#contact">Contacto</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END MENU DESIGN AREA -->
                    </div>
                </div>
            </div>
        </div>
        <div id="slide" class="welcome-image-area">
            <div id="text-slider-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">

                        <div class="single-text-slider-item">
                            <div class="container">
                                <div class="row" style="height: 50%; min-height: 300px;">
                                    <div class="col-md-10 col-md-offset-1">
                                        <h2 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">Desarrollo Web</h2>
                                        <p class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="padding-left: 30px; padding-right: 20px;">Somos especialistas en desarrollo web. Te garantizamos la solución del diseño web ideal para tu empresa. Diseño único, responsive y adaptable a todos los dispositivos.</p>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#service">Más Información</a>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#contact">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="single-text-slider-item">
                            <div class="container">
                                <div class="row" style="height: 50%; min-height: 300px;">
                                    <div class="col-md-10 col-md-offset-1">
                                        <h2 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">Tienda Online</h2>
                                        <p class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="padding-left: 30px; padding-right: 20px;">Realizamos un diseño responsivo a medida y desarrollamos la plataforma e-commerce en la tecnología que mejor cubra las necesidades de tu empresa.</p>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#service">Más Información</a>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#contact">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="left text-slider-control" href="#text-slider-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="right text-slider-control" href="#text-slider-carousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </header>

    <header id="slide-mobile" class="welcome-area">
        <div class="welcome-image-area-mobile">
            <img style="width: 100%;" src="assets/images/bg/slide2.png" alt="agencia de marketing digital">
            <div id="text-slider-carousel" class="carousel slide" data-ride="carousel" style="padding: 20px 0 20px 0">
                <div class="carousel-inner" role="listbox" style="">
                    <div class="item active" style="height:300px">

                        <div class="single-text-slider-item">
                            <div class="container">
                                <div class="row" style="">
                                    <div class="col-md-10 col-md-offset-1">
                                        <h2 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="font-size: 25px;">Desarrollo Web</h2>
                                        <p class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="padding-left: 30px; padding-right: 20px;">Somos especialistas en desarrollo web. Te garantizamos la solución del diseño web ideal para tu empresa. Diseño único, responsive y adaptable a todos los dispositivos.</p>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#service">Más Información</a>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#contact">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>
                    <div class="item" style="height:300px">
                        <div class="single-text-slider-item">
                            <div class="container">
                                <div class="row" style="" >
                                    <div class="col-md-10 col-md-offset-1">
                                        <h2 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="font-size: 25px;">Tienda Online</h2>
                                        <p class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="padding-left: 30px; padding-right: 20px;">Realizamos un diseño responsivo a medida y desarrollamos la plataforma e-commerce en la tecnología que mejor cubra las necesidades de tu empresa.</p>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#service">Más Información</a>
                                        <a class="slide-btn smoth-scroll wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" data-wow-offset="0" href="#contact">Contactar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <!-- START CONTACT DESIGN AREA -->
<section id="contact" class="contact-area section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Contactanos</h2>
                    </div>
                </div>
            </div>
            <div class="row" style="">
                <!-- START CONTACT FORM DESIGN AREA -->
                <div class="col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.1s" data-wow-offset="0">
                    <div class="contact-form">
                        <div class="row">
                            <!-- START CONTACT DETAILS DESIGN AREA -->
                            <div class="contact-details">
                                <!-- <div class="col-md-4">
                                    <div class="single-contact">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <h6 class="text-muted">Buenos Aires, Argentina</h6>
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="single-contact">
                                        <a href="https://api.whatsapp.com/send?text=Hola%20quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20una%20p%C3%A1gina%20web&phone=%2B5491166641537">
                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                            <h6 class="text-muted">+54 9 11 5496-7570</h6>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-contact">
                                        <a href="mailto:hola@wemanagement.com.ar">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </a>
                                        <h6 class="text-muted"><a style="text-decoration:none;" href="mailto:hola@wemanagement.com.ar">hola@wemanagement.com.ar</a></h6>           
                                    </div>
                                </div>
                            </div>
                            <!-- / END CONTACT DETAILS DESIGN AREA -->
                        </div>
                        <div class="row">
                            <!-- START CONTACT FORM DESIGN AREA -->
                            <form id="contact-form" method="POST" enctype="multipart/form-data" action="contact.php">
                            <!-- SOURCE -->
                                <input type="hidden" name="utm_source" value="<?= (isset($_GET['utm_source'])) ? $_GET['utm_source'] : '' ?>">
                                <input type="hidden" name="utm_medium" value="<?= (isset($_GET['utm_medium'])) ? $_GET['utm_medium'] : '' ?>">
                                <input type="hidden" name="utm_campaign" value="<?= (isset($_GET['utm_campaign'])) ? $_GET['utm_campaign'] : '' ?>">
                                <input type="hidden" name="utm_term" value="<?= (isset($_GET['utm_term'])) ? $_GET['utm_term'] : '' ?>">
                                <input type="hidden" name="utm_content" value="<?= (isset($_GET['utm_content'])) ? $_GET['utm_content'] : '' ?>">
                                <input type="hidden" name="pais" value="<?= (isset($_GET['pais'])) ? $_GET['pais'] : '' ?>">
                                
                                <div class="form-group col-md-6">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" required="required">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="required">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="number" name="phone" class="form-control" id="phone" placeholder="Teléfono" required> 
                                </div>
                                <div class="form-group col-md-12">
                                    <select class="form-control" name="web_type" required>
                                        <option value="" selected disabled>Seleccionar tipo de página</option>
                                        <option value="web-informativa">Página web Informativa</option>
                                        <option value="tienda-online">Tienda Online</option>
                                        <option value="no-estoy-seguro">No estoy seguro</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea rows="6" name="message" class="form-control" id="message" placeholder="Mensaje" required="required"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <div class="actions">
                                        <input type="submit" value="Enviar" name="submit" id="submitButton" class="btn btn-lg btn-contact-bg" title="Enviar">
                                    </div>
                                </div>
                            </form>
                            <!-- / END CONTACT FORM DESIGN AREA -->
                        </div>
                    </div>
                </div>
                <div class="col-md-6 no-padding wow fadeInUp" style="margin-top:30px" data-wow-delay=".8s">
                    <!-- START ABOUT US TEXT DESIGN AREA -->
                    <div class="col-md-12 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.1s" data-wow-offset="0">
                        <div class="about-text">
                            <h4 class="text-center">Bienvenido/a <span>We Management</span></h4>
                            <div style="margin-top: 40px;">
                                <p>Somos especialistas en desarrollo web. Te garantizamos la solución del diseño web ideal para tu empresa.</p>
                                <!-- ITEM1 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Diseño único y responsive</p>
                                    </div>
                                </div>
                                <!-- ITEM2 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Adaptable a todos los dispositivos</p>
                                    </div>
                                </div>
                                <!-- ITEM3 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Diseño de imágenes y redacción de textos</p>
                                    </div>
                                </div>
                                <!-- ITEM4 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Contenido dinámico</p>
                                    </div>
                                </div>
                                <!-- ITEM5 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Hosting y mantenimiento</p>
                                    </div>
                                </div>
                                <!-- ITEM6 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Correos corporativos ilímitados </p>
                                    </div>
                                </div>
                                <!-- ITEM7 -->
                                <div class="row" style="display:flex; align-items: center;">
                                    <div class="col-md-2" style="display:flex; justify-content:center;">
                                        <i class="fa fa-check" style="font-size: 50px; color:#2AAFC0;"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <p style="margin: 0 auto">Dominio Gratis</p>
                                    </div>
                                </div>
                            </div>
                            <!-- <p>Ponemos a su disposición un completo menú de servicios, desde el desarrollo de la imagen corporativa de su empresa, diseño de su sitio web, hasta el desarrollo de sistemas de alta complejidad.</p>
                            <p>Realizamos diseño de la identidad corporativa integral, branding, logos, isologos, papelería comercial, tarjetas personales y comerciales. Así como también diseños de presentaciones en formato pdf y diapositivas para reuniones con ilustraciones y gráficos estadísticos.</p>
                            <p>Somos un equipo de profesionales creativos que comprende cómo desarrollar y utilizar el contenido como un medio para aumentar tus ingresos y construir relaciones significativas. Creamos contenido de valor y relevante orientado a generar engagement, loyalty y conversión. Desarrollamos e implementamos un plan táctico de Content Marketing para que la marca pueda construir lazos fuertes a largo plazo con los consumidores de mayor valor.</p> -->
                            <!-- <a href="#service" class="read-more">Más Información</a> -->
                        </div>
                    </div>
                    <!-- / END ABOUT US TEXT DESIGN AREA -->
                    <!-- START MAP DESIGN AREA -->
                    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3288.1404258300404!2d-58.69440028416254!3d-34.49932405949023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbd9390304585%3A0x67498ade7c495722!2sWe%20Management!5e0!3m2!1ses-419!2sar!4v1590458198322!5m2!1ses-419!2sar" width="95%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
                    <!-- / END MAP DESIGN AREA -->
                </div>
                <!-- /END  CONTACT DETAILS DESIGN AREA -->
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->

    <!-- START SERVICES DESIGN AREA -->
    <section id="service" class="service-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title text-center">
                        <h2>¿Cómo lo hacemos?</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay=".2s">
                    <div class="single-service">
                        <i class="fa fa-camera"></i>
                        <h4>Contenido</h4>
                        <p>Planificamos el contenido para tu página, la redacción de los textos y el diseño de las imágenes.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay=".4s">
                    <div class="single-service">
                        <i class="fa fa-laptop"></i>
                        <h4>Diseño</h4>
                        <p>Realizamos un diseño a medida. Cuidamos de cada detalle e incorporamos todo lo necesario para producir una excelente presentación de tu negocio en Internet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay=".6s">
                    <div class="single-service">
                        <i class="fa fa-code"></i>
                        <h4>Desarrollo</h4>
                        <p>Utilizamos herramientas de última generación en HTML, CSS, PHP, JS Y JQUERY.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay=".8s">
                    <div class="single-service">
                        <!-- <i class="fa fa-connectdevelop"></i> -->
                        <i class="fa fa-mobile"></i>
                        <h4>Responsive</h4>
                        <p>Los sitios web que realizamos detectan los diferentes dispositivos y se adaptan al 100% de las resoluciones tanto de pantalla como de los dispositivos móviles como notebooks, tablets y smartphones.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1s">
                    <div class="single-service">
                        <i class="fa fa-flask"></i>
                        <h4>Integraciones</h4>
                        <p>Hacemos cualquier integración. Correos de bienvenida, automatización de contactos, integración de correo electrónico, medios de pago y envío, etc.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1.2s">
                    <div class="single-service">
                        <i class="fa fa-users"></i>
                        <h4>Soporte 24/7</h4>
                        <p>Vas a estar en contacto con nosotros durante todo el proceso.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
            </div>
        </div>
    </section>
    <!-- / END SERVICES DESIGN AREA -->

    <!-- GET STARTED DESIGN AREA -->
    <section class="get-started section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="get-text text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
                        <h2>¿Todavía no tenés página web?</h2>
                        <p>Hoy en día no tener página web es como tener un local cerrado. Te hacemos tu página web única y profesional.</p>
                        <a href="#contact" class="read-more">Empezar</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / END GET STARTED DESIGN AREA -->

    <!-- START WHY CHOOSE US DESIGN AREA -->
    <section class="why-choose-us-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>¿Por qué elegirnos?</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- START WHY CHOOSE US IMAGE DESIGN AREA -->
                <div class="col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s" data-wow-offset="0">
                    <div class="why-chosse-image">
                        <img src="assets/images/nosotros_opt.png" alt="">
                    </div>
                </div>
                <!-- / END WHY CHOOSE US IMAGE DESIGN AREA -->
                <div class="col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s" data-wow-offset="0">
                    <div class="why-choose-us-text">
                        <div class="according-area">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <!-- START SINGLE PANEL DESIGN AREA -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
										<a class="colapsed" role="button" data-toggle="collapse" data-target="#collapseOne" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">
										  Página Web de 0
										</a>
										</h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            Realizamos tu página web de 0. Primero la maquetamos después la diseñamos y la codificamos.
                                        </div>
                                    </div>
                                </div>
                                <!-- / END SINGLE PANEL DESIGN AREA -->
                                <!-- START SINGLE PANEL DESIGN AREA -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										  Contenido Profesional
										</a>
									  </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            Estudiamos tu mercado y competencia para así planificar los textos e imágenes para llamar la atención de tu audiencia y tener mejores resultados.
                                        </div>
                                    </div>
                                </div>
                                <!-- / END SINGLE PANEL DESIGN AREA -->
                                <!-- START SINGLE PANEL DESIGN AREA -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										  Responsive
										</a>
										</h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            Los sitios web que realizamos detectan los diferentes dispositivos y se adaptan al 100% de las resoluciones tanto de pantalla como de los dispositivos móviles.
                                        </div>
                                    </div>
                                </div>
                                <!-- / END SINGLE PANEL DESIGN AREA -->
                                <!-- START SINGLE PANEL DESIGN AREA -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingfour">
                                        <h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
										  Profesionalismo
										</a>
										</h4>
                                    </div>
                                    <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                        <div class="panel-body">
                                            Nos tomamos tu proyecto con profesionalismo, entusiasmo y predisposición para compartir ideas y brindarte un servicio único y profesional.
                                        </div>
                                    </div>
                                </div>
                                <!-- / END SINGLE PANEL DESIGN AREA -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / END WHY CHOOSE US DESIGN AREA -->


    <!-- PROJECT COMPLETE DESIGN AREA -->
    <section class="project-complete-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-project-complete">
                        <i class="fa fa-thumbs-o-up"></i>
                        <p style="color: white; font-size: 60px;">+</p>
                        <h2 class="counter-num">100</h2>
                        <h6 class="text-muted">Proyectos</h6>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-project-complete">
                        <i class="fa fa-users"></i>
                        <p style="color: white; font-size: 60px;">+</p>
                        <h2 class="counter-num">200</h2>
                        <h6 class="text-muted">Clientes Contentos</h6>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-project-complete">
                        <i class="fa fa-code"></i>
                        <p style="color: white; font-size: 60px;">+</p>
                        <h2 class="counter-num">50.000</h2>
                        <h6 class="text-muted">Líneas de Código</h6>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-project-complete">
                        <i class="fa fa-angellist"></i>
                        <p style="color: white; font-size: 60px;">+</p>
                        <h2 class="counter-num">4</h2>
                        <h6 class="text-muted">Años de Experiencia</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / END PROJECT COMPLETE DESIGN AREA -->

    <!-- START WORK DESIGN AREA -->
    <section id="portfolio" class="work section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Portfolio</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="filters col-md-12">
                    <ul id="filters">
                        <li class="active" data-filter="*">All</li>
                        <li data-filter=".web">Web Institucional</li>
                        <li data-filter=".ec">E-commerce</li>
                    </ul>
                </div>
            </div>
            <div class="work-inner">
                <div class="row work-posts grid">
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic ec">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="https://madefordaysoff.com.ar/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/MFDO.png" alt="">
                                    <figcaption>
                                        <h2>Made For Days Off</h2>
                                        <div class="category">
                                            <span>E-commerce Design</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 web">
                        <div class="item wow fadeInUp" data-wow-delay=".6s">
                            <a href="https://www.centromedicosaavedra.com.ar/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/SAAVEDRA1.png" alt="">
                                    <figcaption>
                                        <h2>Centro Médico  Saavedra</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 typography web">
                        <div class="item wow fadeInUp" data-wow-delay="1s">
                            <a href="https://contagram.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/CONTA.png" alt="">
                                    <figcaption>
                                        <h2>Contagram</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <!-- <div class="col-md-4 col-sm-6 graphic photography web">
                        <div class="item wow fadeInUp" data-wow-delay="1.2s">
                            <a href="https://www.restauracioncentral.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/RC.png" alt="">
                                    <figcaption>
                                        <h2>Restauración Central</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 web typography web">
                        <div class="item wow fadeInUp" data-wow-delay="1.4s">
                            <a href="https://peraltayasociados.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/PERALTA.png" alt="">
                                    <figcaption>
                                        <h2>Peralta y Asociados</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 photography web">
                        <div class="item wow fadeInUp" data-wow-delay="1.6s">
                            <a href="https://mvdasesores.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/MVD.png" alt="">
                                    <figcaption>
                                        <h2>MVD Asesores</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 photography web">
                        <div class="item wow fadeInUp" data-wow-delay="1.6s">
                            <a href="http://tommyferlini.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/tommy1_opt.png" alt="">
                                    <figcaption>
                                        <h2>Tommy Ferlini</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic web">
                        <div class="item wow fadeInUp" data-wow-delay="1.8s">
                            <a href="http://impenetrablemj.com.ar/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/IMPENETRABLE.png" alt="">
                                    <figcaption>
                                        <h2>Impenetable MJ</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic ec">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="https://www.di-fab.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/DIFAB.png" alt="">
                                    <figcaption>
                                        <h2>Di-fab</h2>
                                        <div class="category">
                                            <span>E-commerce Design</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic ec">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="https://pyrus.com.ar/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/PYRUS.png" alt="">
                                    <figcaption>
                                        <h2>Pyrus</h2>
                                        <div class="category">
                                            <span>E-commerce Design</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic web">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="https://www.caobamueblesdeco.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/CAOBA.png" alt="">
                                    <figcaption>
                                        <h2>Caoba Muebles</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic ec">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="http://lavidabeauty.life/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/vida.png" alt="">
                                    <figcaption>
                                        <h2>La Vida Beauty</h2>
                                        <div class="category">
                                            <span>E-commerce</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic web">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="http://conquistainterior.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/conquista.png" alt="">
                                    <figcaption>
                                        <h2>Conquista Interior</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic web">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="http://jinglesparapublicidad.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/jingles.png" alt="">
                                    <figcaption>
                                        <h2>Jingles CLV Pro</h2>
                                        <div class="category">
                                            <span>Web Institucional</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-6 graphic ec">
                        <div class="item wow fadeInUp" data-wow-delay=".4s">
                            <a href="http://mzventadirecta.com/" target="_blank">
                                <figure class="effect-apollo">
                                    <img src="assets/images/work/mz.png" alt="">
                                    <figcaption>
                                        <h2>MZ Venta Directa</h2>
                                        <div class="category">
                                            <span>E-commerce</span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                </div>
            </div>
            <p class="text-center">y muchos proyectos más</p>

        </div>
        
    </section>
    <!-- / END START WORK DESIGN AREA -->

    <!-- START FOOTER TOP DESIGN AREA -->
    <section class="footer-top-area section-padding">
        <div class="container">
            <div class="row">
                <!-- START SINGLE FOOTER DESIGN AREA -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-footer wow fadeInUp" data-wow-delay=".2s">
                        <h2>Nosotros</h2>
                        <p>Somos un equipo de profesionales creativos que comprende cómo desarrollar y utilizar el contenido como un medio para aumentar tus ingresos y construir relaciones significativas. Creamos contenido de valor y relevante orientado a generar engagement, loyalty y conversión.</p>
                    </div>
                </div>
                <!-- / END SINGLE FOOTER DESIGN AREA -->
                <!-- START SINGLE FOOTER DESIGN AREA -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-footer wow fadeInUp" data-wow-delay=".4s">
                        <h2>Footer menu</h2>
                        <div class="footer-menu">
                            <ul>
                                <li><a href="#home">Inicio</a>
                                </li>
                                <li><a href="#service">Servicios</a>
                                </li>
                                <li><a href="#portfolio">Portoflio</a>
                                </li>
                                <li><a href="#contact">Contacto</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / END SINGLE FOOTER DESIGN AREA -->
                <!-- START SINGLE FOOTER DESIGN AREA -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-footer wow fadeInUp" data-wow-delay=".6s">
                        <h2>Servicios:</h2>
                        <ol>
                            <li>Diseño Web Adaptable</li>
                            <li>Diseño Web E-commerce</li>
                            <li>Software a Medida</li>
                            <li>Publicidad Digital</li>
                            <li>Community Management</li>
                            <li>Diseño Gráfico</li>
                        </ol>
                    </div>
                </div>
                <!-- / END SINGLE FOOTER DESIGN AREA -->
                <!-- START SINGLE FOOTER DESIGN AREA -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-footer wow fadeInUp" data-wow-delay=".8s">
                        <h2>Redes Sociales</h2>
                        <div class="footer-social-link">
                            <ul>
                                <li><a href="https://www.facebook.com/wemanagement.ok"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href="https://www.instagram.com/wemanagement.ok/"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li><a href="https://www.linkedin.com/company/wemanagement/"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
                <!-- / END SINGLE FOOTER DESIGN AREA -->
            </div>
        </div>
    </section>
    <!-- / END FOOTER TOP DESIGN AREA -->

    <!-- START FOOTER DESIGN AREA -->
    <footer class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="footer-text wow fadeInUp" data-wow-delay=".6s">
                        <p class="text-muted">&copy;copyright 2020 Todos los derechos reservados.
                            <br>Developed by<i class="fa fa-heart"></i> <span><a href="https://wemanagement.com.ar/">We Management</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- / END CONTACT DETAILS DESIGN AREA -->
    <!-- LATEST JQUERY -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- BOOTSTRAP JS -->
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- OWL CAROUSEL JS  -->
    <script src="assets/owlcarousel/js/owl.carousel.min.js"></script>
    <!-- STEALLER JS -->
    <script src="assets/js/jquery.stellar.min.js"></script>
    <!-- COUNTER UP JS -->
    <script src="assets/js/jquery.waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <!-- ISOTOP JS -->
    <script src="assets/js/isotope.pkgd.js"></script>
    <!-- STICKY JS -->
    <script src="assets/js/jquery.sticky.js"></script>
    <!-- scripts js -->
    <script src="assets/js/scripts.js"></script>
    <!-- Contact from script -->
    <script src="assets/js/contact.js"></script>

    
    
  </body>
</html>